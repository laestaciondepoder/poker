<?php

// src/Victortestmaster/Poker/PokerHelper.php
namespace App\Victortestmaster\Poker;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Victortestmaster\Poker\CardsModel;


class PokerHelper extends Bundle
{

private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $this->cardsmodel  = new CardsModel($session);
        $this->draftmodel  = new DraftModel($session);

    }


   /* @action: Starts the game */
    public function play()
	{
                    $drafted_card = $this->draftmodel->draftCards($this->cardsmodel->createCards());
                    $this->cardsmodel->setCard($drafted_card);
                    $message = $this->cardsmodel->findCard($drafted_card);	

		   return array("drafted_card"=>$drafted_card,"message"=>$message);
	}
}
