<?php

// src/Victortestmaster/Poker/DraftModel.php
namespace App\Victortestmaster\Poker;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Victortestmaster\Poker\CardsModel;


class DraftModel extends Bundle
{


private $session;


private  $card;
private  $savecards;
private  $countcards;
private  $classpoker;
private  $used_cards;

    public function __construct(SessionInterface $session)
    {

        $this->cardsmodel  = new CardsModel($session);
        $this->session = $session;


    }


/* @param array()
*/
    public  function draftCards($cards)
    {
	    do {
		    $n = rand(0,count($cards)-1);

	    } while(in_array($n, $this->session->get('used_cards')));

	    $this->getNotusedRandomCard($cards,$n);
	    $this->cardsmodel->saveCard($cards[$n]);
	    return $cards[$n];
    }



    private function getNotusedRandomCard($cards,$n)
    {
	    $this->session->set('used_cards',array_merge($this->session->get('used_cards'),array($n)));
    }


    public static function testdraftCards()
	{
		return $this->draftCards($cards);
	}
}
