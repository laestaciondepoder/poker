<?php

// src/Victortestmaster/Poker/Modelpoker.php
namespace App\Victortestmaster\Poker;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class Modelpoker extends Bundle
{


private $session;


private  $card;
private  $savecards;
private  $countcards;
private  $classpoker;

    public function __construct(SessionInterface $session)
    {

      $this->session = $session;
    }


	public function setCard($card)
	{
		$_SESSION['card'] = $card;
		$this->card = $card;
	}

       public function saveCard($card)
        {
		$add = true;
		foreach($this->session->get('saved_cards') as $saved_cards)
			{
				if(($saved_cards["suit"]==$card["suit"]) && ($saved_cards["value"]==$card["value"]))
					{
						$add = false;
					}
			}
		if($add == true)
			{
                		$this->session->set('saved_cards',array_merge($this->session->get('saved_cards'),array(array("suit"=>$card["suit"],"value"=>$card["value"]))));
                		$this->setSaveCard($this->session->get('saved_cards'));
			}
                return $this->session->get('saved_cards');
        }


	public function setSaveCard($card)
	{
		$this->savecards=$card;
	}

	public function getSavedCards()
	{
		return $this->savecards;
	}

	public function setCountCards($param)
	{
		return $this->countcards = $param;
	}

	public function getCountCadrds()
	{
		return $this->countcards;
	}


}
