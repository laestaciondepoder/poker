<?php

//  Victortestmaster/Poker/PokerTest.php

namespace App\Victortestmaster\Poker;

use App\Util\PokerTest;
use PHPUnit\Framework\TestCase;
use App\Controller\PockerController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PokerTests extends TestCase
{

private $session;

   /*public function setUp(SessionInterface $session)
    {
        $this->session = $session;
	$this->cardsmodel = new CardsModel($session);
    }*/


/*
   public function testIsChanceValid(){

      $cards = \App\Victortestmaster\Poker\CardsModel::createCards();


      $drafted_card = \App\Victortestmaster\Poker\DraftModel::testdraftCards($cards);
      $set_card = \App\Victortestmaster\Poker\CardsModel::setCard($drafted_card);
      $message = \App\Victortestmaster\Poker\CardsModel::findCard($drafted_card);

      $this->assertTrue($game->getChance() == '7.69%');
      unset($game);
  }
*/
    public function testIsValidCardDeck()
    {

        $suits = array(
            'H' => 'Hearts',
            'C' => 'Clubs',
            'D' => 'Diamonds',
            'S' => 'Spades'
        );
        $values = array(
            'A' => 'Ace',
            '2' => 'Two',
            '3' => 'Three',
            '4' => 'Four',
            '5' => 'Five',
            '6' => 'Six',
            '7' => 'Seven',
            '8' => 'Eight',
            '9' => 'Nine',
            '10' => 'Ten',
            'J' => 'Jack',
            'Q' => 'Queen',
            'K' => 'King'
        );

        // 2 dim. card array
        $cards = array();

        // create card array as 2 dim. array
        foreach ($suits as $key => $suit) {

            foreach ($values as $key2 => $value) {
                $cards[] = array(
                    'suit' => $key,
                    'value' => $key2,
                );
            }
        }

        $this->assertTrue(\App\Victortestmaster\Poker\CardsModel::createCards() == $cards);
        unset($game);

    }

}



