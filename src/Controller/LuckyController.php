<?php


// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function SebastianBergmann\ObjectGraph\object_graph_dump;

class LuckyController
{
    /**
     * @Route("/lucky/number/", name="app_lucky_number")
     */
    public function number()
    {
        $number = random_int(0, "120");
	
	return new Response($this->analytics());
        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }



    public function analytics()
    {
	    $param = "football vs soccaor";
	    $param_len = strlen($param);
	    //$param = "fotbaoff";
	    $array = str_split($param);
	    $array_full = array();	
	    $count = 1;

	    foreach($array as $letters)
	    {
		    $array_full = array_merge($array_full,array($count=>$letters));
	    }

	    $longest = 0;
	    $vals = array_count_values($array_full);
	    //print_r($vals);
	    $position_before = '';
	    $best_distance = 0;
	    $max_distance_letter=array();
	    $grid_total = array();
	    foreach($vals as $needle => $i)
	    {
		    $offset = 0;
		    $allpos = array();
		    $position =0;
		    while (($pos = strpos($param, $needle, $offset)) !== FALSE) {
			    if($position>=1)
			    {
				    $position_before = $position_before + 1;
				    $distance = $pos - $position_before;
				    if($distance>$best_distance)
				    {
					    $best_distance=$distance;
					    $max_distance_letter = $needle;
				    }

				    $position_before = $pos;
			    }
			    else
			    {
				    $position_before = $pos;
			    }
			    $offset   = $pos + 1;
			    $allpos[] = $pos;
			    $position++;
		    }
		    
		    $grid = array("letter"              =>$needle,"qty"=>$i
				  ,"max_distance_letter"=>$max_distance_letter
				  ,"qty_chars"          =>$best_distance);
		    //echo $needle.":".$i.":before:";
		    $before = "";
		    foreach($allpos as $ps)
		    {
			    $key = $ps + 1;
			    if($param_len > $key)
			    {
				    $before.=$param[$key]; 
			    }
		    }
		    $grid = array_merge($grid,array("before"=>$before));


		    $after = "";
		    foreach($allpos as $ps)
		    {
			    $key = $ps - 1;
			    $after.=($key==-1)?"none":$param[$ps-1];
		    }
		   $grid = array_merge($grid,array("after"=>$after));
		   $grid_total = array_merge($grid_total,array($grid));
		   //$this->setGrid($grid,$max_distance_letter);
	    }
		echo $this->setGrid($grid_total); 

    } 


	public function setGrid($grid)
		{
$cart = new ShoppingCart;
$cart->add(new ShoppingCartItem('Foo', new Money(123, new Currency('EUR')), 1));
$cart->add(new ShoppingCartItem('Bar', new Money(456, new Currency('EUR')), 1));

object_graph_dump('graph.png', $cart);
		$return='';
			foreach($grid as $row)
				{
					$last = ($row['letter']==$row['max_distance_letter'])?" max-distance: ".$row['qty_chars']." chars ":"";
					$return.=$row['letter']." : ".$row['qty']." : before:".$row['before']." after:".$row['after'].$last."<br>";
				}
		return $return;

		}


}



